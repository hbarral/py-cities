<?php

namespace App\Exceptions;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait ExceptionTrait
{
    public function apiException($e, $request)
    {
        if ($e instanceof ModelNotFoundException) {
            return response()->json([
                'ok' => false,
                'message' => 'Object not found',
                'data' => null,
            ], Response::HTTP_NOT_FOUND);
        }

        if ($e instanceof NotFoundHttpException) {
            return response()->json([
                'ok' => false,
                'message' => 'Route not found',
                'data' => null,
            ], Response::HTTP_NOT_FOUND);
        }
    }
}
