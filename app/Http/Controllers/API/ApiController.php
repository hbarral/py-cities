<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;

const NOT_FOUND = 'NOT_FOUND';
const FORBIDDEN = 'FORBIDDEN';
const SOMETHING_WENT_WRONG = 'SOMETHING_WENT_WRONG';
const BAD_REQUEST = 'BAD_REQUEST';
const DELETED = 'DELETED';
const UNAUTHORIZED = 'UNAUTHORIZED';

class ApiController extends Controller
{
    protected $statusCode = Response::HTTP_OK;

    protected $parameters = [];

    public function __construct()
    {
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function setStatusCode($status)
    {
        $this->statusCode = $status;

        return $this;
    }

    public function respondOK($data = null, $message = null, $header = [], $code = Response::HTTP_OK)
    {
        return $this->setStatusCode($code)->respond([
          'ok' => true,
          'message' => $message,
          'data' => $data,
        ], $header);
    }

    public function respondCreated($data = null, $message = null, $header = [])
    {
        $code = Response::HTTP_CREATED;

        return $this->setStatusCode($code)->respond([
          'ok' => true,
          'message' => $message,
          'data' => $data,
        ], $header);
    }

    public function respondNotFound($message = NOT_FOUND)
    {
        return $this->respondWithError(Response::HTTP_NOT_FOUND, $message);
    }

    public function respondWithError($error = Response::HTTP_BAD_REQUEST, $message = null, $data = null)
    {
        logger()->error($error);
        logger()->error($message);

        return $this->setStatusCode($error)->respond([
          'ok' => false,
          'message' => $message ?? BAD_REQUEST,
          'data' => $data,
        ]);
    }

    public function respond($data, $headers = [])
    {
        return response()->json($data, $this->getStatusCode(), $headers);
    }

    public function respondInternalError()
    {
        return $this->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR)->respond([
          'ok' => false,
          'message' => SOMETHING_WENT_WRONG,
          'data' => null,
        ]);
    }

    public function respondBadRequest($message = null)
    {
        return $this->setStatusCode(Response::HTTP_BAD_REQUEST)->respond([
          'ok' => false,
          'message' => $message ?? BAD_REQUEST,
          'data' => null,
        ]);
    }

    public function respondForbidden()
    {
        return $this->setStatusCode(Response::HTTP_FORBIDDEN)->respond([
          'ok' => false,
          'message' => FORBIDDEN,
          'data' => null,
        ]);
    }

    public function respondUnauthorized($message = UNAUTHORIZED)
    {
        return $this->setStatusCode(Response::HTTP_UNAUTHORIZED)->respond([
          'ok' => false,
          'message' => $message,
          'data' => null,
        ]);
    }

    public function respondDeleted($message = DELETED)
    {
        return $this->setStatusCode(Response::HTTP_OK)->respond([
          'ok' => true,
          'message' => $message,
          'data' => null,
        ]);
    }
}
