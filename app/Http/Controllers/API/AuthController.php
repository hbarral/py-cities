<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

const TASHCRM_TOKEN_NAME = 'TASHCRM_TOKEN';

class AuthController extends ApiController
{
    public function signup(Request $request)
    {
        $validated = $request->validate([
            'email' => 'required|email|unique:users,email|filled',
            'name' => 'required|string|max:255|filled',
            'password' => 'required|confirmed|string|filled|min:8',
        ]);

        $validated['password'] = bcrypt($validated['password']);

        try {
            $user = User::create($validated);

            $token = $user->createToken(TASHCRM_TOKEN_NAME)->plainTextToken;

            $token_type = 'Bearer';
        } catch (\Throwable $th) {
            throw $th;
        }

        return $this->respondOK(data: [
            'token' => $token,
            'token_type' => $token_type,
            'user' => $user,
        ], message: 'user created', code: Response::HTTP_CREATED);
    }

    public function signin(Request $request)
    {
        [
            'email' => $email,
            'password' => $password,
        ] = $request->validate([
            'email' => 'required|email|filled',
            'password' => 'required|filled',
        ]);

        $user = User::whereEmail($email)->first();

        if (! $user || ! Hash::check($password, $user->password)) {
            return $this->respondUnauthorized();
        }

        $token = $user->createToken(TASHCRM_TOKEN_NAME)->plainTextToken;

        $token_type = 'Bearer';

        return $this->respondOK(
            data: [
                'user' => $user,
                'token' => $token,
                'token_type' => $token_type,
            ],
            message: 'successful login');
    }

    public function signout()
    {
        auth()->user()->tokens()->delete();

        return $this->respondOK(message: 'signed out');
    }
}
