<?php

namespace App\Http\Controllers;

use App\Http\Controllers\API\ApiController;
use App\Models\Department;
use Illuminate\Http\Request;

class DepartmentController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validated = $request->validate([
            'name' => 'string|nullable|max:32',
            'page' => 'alpha_num|nullable',
            'per_page' => 'numeric|nullable',
        ]);

        $name = $validated['name'] ?? '';
        $page = $validated['page'] ?? 'all';
        $per_page = $validated['per_page'] ?? 5;

        $departments = Department::where([
            ['name', 'like', '%' . $name . '%'],
        ]);

        if ('all' === $page) {
            $departments = $departments->get();
        }

        if ('all' !== $page) {
            $departments = $departments->paginate($per_page);
        }

        return $this->respondOK(['departments' => $departments]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (auth()->user()->type !== 'admin') {
            return $this->respondUnauthorized('Are you sure you are the King?');
        }

        $validated = $request->validate([
            'name' => 'required|filled|string|max:64',
        ]);

        $department = Department::updateOrCreate($validated);

        return $this->respondCreated(['department' => $department ], 'Congratulations! You have conquered new lands!');
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        return $this->respondOK(['department' => $department], 'These lands are still under your control.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department)
    {
        if (auth()->user()->type !== 'admin') {
            return $this->respondUnauthorized('Something tells me you don\'t have enough power');
        }

        $validated = $request->validate([
            'name' => 'required|filled|string|max:64',
        ]);

        $department->update($validated);

        return $this->respondCreated(['department' => $department], 'Congratulations! This land continues to prosper!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
        if (auth()->user()->type !== 'admin') {
            return $this->respondUnauthorized('Such power of destruction requires the presence of Your Highness');
        }

        $department->delete();

        return $this->respondDeleted('Sorry, you have lost part of your land');
    }
}
