<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DepartmentTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function setup() : void
    {
        parent::setup();
        $this->artisan('db:seed');
    }

    // Authenticated user

    /**
    * An authenticated user can see the list of departments.
    *
    * @test
    *
    * @return void
    */
    public function an_authenticated_user_can_see_the_list_of_departments()
    {
        $this->logInUser();

        $response = $this->get('/api/department');

        $res = $response->getOriginalContent();

        $departments = $res['data']['departments']->toArray();

        $departmentsShouldExists = [
            'ALTO PARAGUAY',
            'ALTO PARANÁ',
            'AMAMBAY',
            // 'ASUNCIÓN',
            'BOQUERÓN',
            'CAAGUAZÚ',
            'CAAZAPÁ',
            'CANINDEYÚ',
            'CENTRAL',
            'CONCEPCIÓN',
            'CORDILLERA',
            'GUAIRÁ',
            'ITAPÚA',
            'MISIONES',
            'PARAGUARÍ',
            'SAN PEDRO',
            'ÑEEMBUCÚ',
        ];

        $exists = true;
        foreach ($departmentsShouldExists as $name) {
            if (! array_search($name, array_column($departments, 'name'))) {
                $exists = false;
                break;
            }
        }

        $this->assertTrue($exists, "actual value is not equals to expected");
    }

    /**
    * A non-admin user cannot create a department.
    *
    * @test
    *
    * @return void
    */
    public function a_non_admin_user_cannot_create_a_department()
    {
        $this->logInUser();

        $new_department = $this->faker->words(2, true);

        $toCreate = [
            'name' => $new_department,
        ];

        $response = $this->post('/api/department', $toCreate);

        $response->assertJson([
            'ok' => false,
            'message' => 'Are you sure you are the King?',
            'data' => null,
        ], true);

        $this->assertDatabaseMissing('departments', ['name' => $new_department]);
    }

    // Authenticated admin

    /**
     * An admin user can create a Department.
     *
     * @test
     *
     * @return void
     */
    public function an_admin_user_can_create_a_department()
    {
        $this->logInUser(['type' => 'admin']);

        $new_department = $this->faker->words(2, true);

        $toCreate = [
            'name' => $new_department,
        ];

        $response = $this->post('/api/department', $toCreate);

        $response->assertJson([
            'ok' => true,
            'message' => 'Congratulations! You have conquered new lands!',
            'data' => [
                'department' => [
                    'name' => $new_department,
                ],
            ],
        ], true);

        $this->assertDatabaseHas('departments', ['name' => $new_department]);
    }


    // Unauthenticated user

    /**
    * An unauthenticated user cannot see the list of departments.
    *
    * @test
    *
    * @return void
    */
    public function an_unauthenticated_user_cannot_see_the_list_of_departments()
    {
        $this->withoutExceptionHandling();
        $this->expectException('Illuminate\Auth\AuthenticationException');
        $this->getJson('/api/department');
    }
}
