# PY-Cities API

## Project Overview
This project aims to list the departments, cities and towns of Paraguay through an API that requires authentication, built with the support of the PHP Laravel framework.

## Configuration instructions
A computer with Linux system (It should work on Windows and Mac as well) with PHP, Nginx, MariaDB or alternatives.

## Technologies
* PHP 8.1.7
* Nginx 1.22.0
* MariaDB 10.8.3

## Installation instructions
* Clone project
```bash
git clone git@gitlab.com:hbarral/py-cities.git py-cities
```

* cd into 'py-cities'
```bash
cd py-cities
```

* Install composer packages
```bash
composer install
```

* Copy .env file
```bash
cp .env.example .env
```

* Edit the '.evn' file according to your requirements

* Generate key
```bash
php artisan key:generate
```

* Publish the Sanctum configuration and migration
```bash
php artisan vendor:publish --provider="Laravel\Sanctum\SanctumServiceProvider"
```

* Run migrations and seed
```bash
php artisan migrate --seed
```

* Run dev server
```bash
php artisan serve
```

> Now you can use the 'Postman collection' provided in the 'xtras' folder to start using the API.
