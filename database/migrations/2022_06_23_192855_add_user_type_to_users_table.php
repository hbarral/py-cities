<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class() extends Migration {
    public function up()
    {
        Schema::table((new User())->getTable(), function (Blueprint $table) {
            $table->string('type')->after('remember_token')->nullable();
        });
    }

    public function down()
    {
        $table = (new User())->getTable();
        if (Schema::hasColumn($table, 'type')) {
            Schema::table($table, function (Blueprint $table) {
                $table->dropColumn('type');
            });
        }
    }
};
