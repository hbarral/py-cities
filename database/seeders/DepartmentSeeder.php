<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Department;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departments = [
            [
                'name' => 'ASUNCIÓN',
            ],
            [
                'name' => 'CONCEPCIÓN',
            ],
            [
                'name' => 'SAN PEDRO',
            ],
            [
                'name' => 'CORDILLERA',
            ],
            [
                'name' => 'GUAIRÁ',
            ],
            [
                'name' => 'CAAGUAZÚ',
            ],
            [
                'name' => 'CAAZAPÁ',
            ],
            [
                'name' => 'ITAPÚA',
            ],
            [
                'name' => 'MISIONES',
            ],
            [
                'name' => 'PARAGUARÍ',
            ],
            [
                'name' => 'ALTO PARANÁ',
            ],
            [
                'name' => 'CENTRAL',
            ],
            [
                'name' => 'ÑEEMBUCÚ',
            ],
            [
                'name' => 'AMAMBAY',
            ],
            [
                'name' => 'CANINDEYÚ',
            ],
            [
                'name' => 'PRESIDENTE HAYES',
            ],
            [
                'name' => 'BOQUERÓN',
            ],
            [
                'name' => 'ALTO PARAGUAY',
            ],
        ];

        Department::insert($departments);
    }
}
